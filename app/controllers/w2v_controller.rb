class W2vController < ApplicationController
  def new
  	@w2v = W2v.new
  end

  def create
  	@w2v = W2v.new(w2v_params)
  	@w2v.save
  	redirect_to new_w2v_path
  end

  def w2v_params
  	params.require(:w2v).permit(:theme)
  end
end
